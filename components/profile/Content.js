export default function Content() {
  return (
    <div>
      <h1 className="display-4 mt-4">Permainan</h1>
      <p className="lead">
        yang telah dimainkan
      </p>
      <hr className="my-2" />
    </div>
  );
}
