import {useEffect, useState} from "react";
import {Table} from "reactstrap";
import {connect} from "react-redux";
import Image from "next/image";

function UserDetail(props) {
  const [dataUser, setDataUser] = useState([]);
  const [score, setScore] = useState([]);
  const {scoreRedux2} = props;

  const getUser = () => {
    const token = localStorage.getItem("token");
    if (token) {
      const user = JSON.parse(localStorage.getItem("user"));
      setDataUser(user);
    }
  };

  useEffect(() => {
    getUser();
    const token = localStorage.getItem("token");
    const _score = localStorage.getItem("score");
    if (token) {
      if (_score === null) {
        setScore(0);
      } else {
        setScore(localStorage.getItem("score"));
      }
    }
    if (score !== scoreRedux2 && scoreRedux2 !== -1) {
      localStorage.setItem("score", scoreRedux2);
      setScore(scoreRedux2);
    }
  }, [scoreRedux2, score]);
  return (
    <>
      <div className="mt-4 mx-auto text-center">
        {dataUser.photoURL ? (
          <Image src={dataUser.photoURL} className="rounded-circle" alt="Profile" width={250} height={250} />
        ) : (
          <Image src="/user.png" className="rounded-circle" alt="Profile" width={250} height={250} />
        )}
        <div className="justify-item-center">
          <div className="d-flex">
            <h3>{dataUser.displayName}</h3>
          </div>
        </div>
      </div>
      <Table borderless responsive size="sm" className="text-mode">
        <tbody>
          <tr>
            <th scope="row">Email</th>
            <td>: {dataUser.email}</td>
          </tr>
          <tr>
            <th scope="row">RPS Score</th>
            <td>: {scoreRedux2 === -1 ? score : score !== scoreRedux2 && scoreRedux2 >= 0 ? scoreRedux2 : scoreRedux2} point</td>
          </tr>
        </tbody>
      </Table>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    scoreRedux: state.auth.scoreRedux,
    scoreRedux2: state.game.scoreRedux,
  };
};

export default connect(mapStateToProps)(UserDetail);
