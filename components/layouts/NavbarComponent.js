import {Collapse, Navbar, NavbarToggler, Nav, NavItem, Badge} from "reactstrap";
import Link from "next/link";
import {useState, useEffect} from "react";
import {connect} from "react-redux";
import {signOut} from "../../redux/actions/authActions";
import Image from "next/image";

function NavbarComponent(props) {
  const [isOpen, setIsOpen] = useState(false);
  const [displayName, setDisplayName] = useState("");
  const [score, setScore] = useState("");
  const [photoURL, setPhotoURL] = useState("");
  const [uid, setUid] = useState("");
  const [iconSrc, setIconSrc] = useState("/switch/moon.png");
  const {scoreRedux2} = props;

  // const user = auth.currentUser;

  useEffect(() => {
    const token = localStorage.getItem("token");
    const test = JSON.parse(localStorage.getItem("user"));
    const _score = localStorage.getItem("score");
    if (token) {
      setDisplayName(test.displayName);
      setPhotoURL(test.photoURL);
      setUid(test.uid);
      if (_score === null) {
        setScore(0);
      } else {
        setScore(localStorage.getItem("score"));
      }
    }
    if (score !== scoreRedux2 && scoreRedux2 !== -1) {
      localStorage.setItem("score", scoreRedux2);
      setScore(scoreRedux2);
    }
    if (document.body.classList.contains("dark-theme")) {
      setIconSrc("/switch/sun.png")
    } else {
      setIconSrc("/switch/moon.png")
    }
  }, [score, scoreRedux2]);

  const changeToggle = () => {
    setIsOpen(!isOpen);
  };
  const switchColorMode = () => {
    const lightIcon = "/switch/sun.png"
    const darkIcon = "/switch/moon.png"
    document.body.classList.toggle("dark-theme");
    if (document.body.classList.contains("dark-theme")) {
      setIconSrc(lightIcon)
    } else {
      setIconSrc(darkIcon)
    }
  }
  const Ternary = () => {
    if (displayName !== null && displayName !== "") {
      return (
        <>
          {photoURL ? (
            <NavItem>
              <Image src={photoURL} className="rounded-circle" alt="Profile" width={40} height={40} />
            </NavItem>
          ) : (
            <NavItem>
              <Image src="/user.png" className="rounded-circle" alt="Profile" width={40} height={40} />
            </NavItem>
          )}
          <NavItem>
            <Link href={"/profile/" + [uid]}>
              <a className="nav-link">
                {displayName}
                <span className="score_info">
                  <Badge color="danger" pill>
                    {scoreRedux2 === -1 ? score : score !== scoreRedux2 && scoreRedux2 >= 0 ? scoreRedux2 : scoreRedux2}
                  </Badge>
                </span>
              </a>
            </Link>
          </NavItem>
          <NavItem>
            <a href="#" onClick={props.signOut} className="nav-link">
              Logout
            </a>
          </NavItem>
        </>
      );
    } else {
      return (
        <>
          <NavItem>
            <Link href="/login">
              <a className="nav-link">Login</a>
            </Link>
          </NavItem>
          <NavItem>
            <Link href="/register">
              <a className="nav-link">Register</a>
            </Link>
          </NavItem>
        </>
      );
    }
  };
  return (
    <div className="px-lg-5 shadow bg_navbar navbar-dark rounded fixed-top">
      <Navbar expand="md">
        <Link href="/">
          <a className="navbar-brand">K2</a>
        </Link>
        <NavbarToggler onClick={changeToggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem>
              <Link href="/">
                <a className="nav-link">Home</a>
              </Link>
            </NavItem>
            <NavItem>
              <Link href="/games/rps">
                <a className="nav-link">Play</a>
              </Link>
            </NavItem>
            <div className="icon-switch mx-3 my-auto" color="light" size="sm" onClick={switchColorMode}>
              <Image
                src={iconSrc}
                alt="color-mode" width={25} height={25} />
            </div>
          </Nav>
          <Nav className="ml-auto" navbar>
            <Ternary />
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    token: state.auth.token,
    scoreRedux: state.auth.scoreRedux,
    scoreRedux2: state.game.scoreRedux,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavbarComponent);
