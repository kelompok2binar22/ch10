import React from 'react';
const Footer = () => {
 
  return (
  <section className="footer section">
    <div className="text-center text-muted">
    <p className="mb-0 f-15">2022 © Kelompok 2</p>
    </div>
   </section>
  );
}
export default Footer;