import {initializeApp} from "firebase/app";
import {getAuth} from "firebase/auth";
import {getFirestore} from "firebase/firestore";
import {getStorage} from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCI6ZyWeQr67BWLo5ITD1TipQZtxLjuOYc",
  authDomain: "ch10-59466.firebaseapp.com",
  projectId: "ch10-59466",
  storageBucket: "ch10-59466.appspot.com",
  messagingSenderId: "1010344049177",
  appId: "1:1010344049177:web:401ff53a052eb1325f4888"
};

const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);
