import {Row, Col, Container} from "reactstrap";
import Layout from "../../components/layouts/Layout";
import GameRPS from "../../components/profile/GameRPS";
import Content from "../../components/profile/Content";
import UserDetail from "../../components/profile/UserDetail";
import Router from 'next/router'
import {useEffect} from "react";

export default function Profile() {
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      Router.push("/login");
    }
  }, []);
  return (
    <div>
      <Layout title="Profile">
        <Container>
          <Row className="mt-5 justify-content-center">
            <Col xs="auto" md="4" lg="4" className="invoicePages">
              <UserDetail />
            </Col>
            <Col xs="auto" lg="8" className="invoicePages">
              <Content />
              <GameRPS />
            </Col>
          </Row>
          
          <br />
          <br />
        </Container>
      </Layout>
    </div>
  );
}
