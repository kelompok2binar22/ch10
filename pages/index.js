import Layout from "../components/layouts/Layout";
import GameList from "../components/GameList";
import {Container} from "reactstrap";

export default function LandingPage() {
  return (
    <Layout title="K2">
      <Container className="pt-4">
      <GameList></GameList>
      </Container>
    </Layout>
  );
}
